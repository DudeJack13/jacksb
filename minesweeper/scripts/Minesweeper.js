class Minesweeper {
    header = null;
    mainBoard = null;
    placeFlag = false;
    gameState = "playing";
    constructor(x, y, bombs) {
        var viewportWidth = $(window).width();
        var viewportHeight = $(window).height() - 50;
        if (viewportHeight > viewportWidth) {
            this.header = new Header(bombs);
            this.mainBoard = new Board(x, y, bombs);
        } else {
            this.header = new Header(bombs);
            this.mainBoard = new Board(x, y, bombs);
        }
        this.OnResize();
    }

    SquareClickInput(item) {
        if (this.gameState == "playing") {
            var i = this.mainBoard.OnClickInput(item, this.placeFlag);
            if (i == 2) {
                this.gameState = "end";
            } else if (i == 1 || i == -1) {
                this.header.UpdateFlagCount(i);
            }
        }
        if (this.mainBoard.CheckVictory()) {
            this.gameState = "end";
            this.End();
        }
    }

    FlagButtonClick(item) {
        this.placeFlag = this.header.OnButtonClick(item, this.placeFlag);
        if (this.mainBoard.CheckVictory()) {
            this.End();
        }
    }

    ResetButtonClick() {
        this.gameState = "playing";
        this.mainBoard.ResetAll();
        this.OnResize();
        this.header.SetFlagCount();
    }

    OnResize() {
        var viewportWidth = $(window).width();
        var viewportHeight = $(window).height() - 50;
        if (viewportHeight > viewportWidth) {
            this.mainBoard.OnResize(viewportWidth, viewportWidth);
            this.header.OnResize(viewportWidth, viewportWidth);
        } else {
            this.mainBoard.OnResize(viewportHeight, viewportHeight);
            this.header.OnResize(viewportHeight, viewportHeight);
        }
    }

    End() {
        this.mainBoard.SquareDance();
    }
}