class Header {
    height = 0;
    width = 0;
    flagCount = 0;
    flagButton = null;
    bombCount = 0;
    h1Div = null;
    resetButton = null;
    constructor(bombCount) {
        this.bombCount = bombCount;
        this.flagCount = bombCount;
        this._CreateHeader();
    };

    _CreateHeader() {
        var mainEl = document.getElementById('bC');
        var div = document.createElement("div");
        mainEl.appendChild(div);
        div.classList.add("headerBar");
        $(".headerBar").css({
            'height': '50px',
            'width': String(this.width),
            'display': 'grid',
            'grid-template-columns': '25% 50% 25%',
            'grid-template-rows': '100%',
            'color': 'white',
            'text-align': 'center',
        });

        this.h1Div = document.createElement('div');
        this.h1Div.classList.add("hDiv");

        this.flagButton = document.createElement("button");
        this.flagButton.classList.add("hButton");
        this.flagButton.setAttribute("onClick", "flagClicked(this);");

        this.resetButton = document.createElement("button");
        this.resetButton.classList.add("hButton");
        this.resetButton.setAttribute("onClick", "resetClicked(this);");

        div.appendChild(this.h1Div);
        div.appendChild(this.flagButton);
        div.appendChild(this.resetButton);

        this.h1Div.innerHTML = "<i class='fas fa-flag'></i> " + String(this.flagCount);
        $(".hDiv").css({
            'margin': 'auto',
        });

        this.flagButton.innerHTML = "<i class='fas fa-flag'></i>";
        this.resetButton.innerHTML = "<i class='fas fa-redo'></i>";
        $(".hButton").css({
            'margin': 'auto',
            'padding': '5px 10px 5px 10px',
        });
    }

    OnResize(height, width) {
        this.width = width;
        this.height = height;
        $(".headerBar").css({
            'width': String(this.width),
        });
    }

    OnButtonClick(item, placeFlag) {
        if (placeFlag) {
            item.innerHTML = "<i class='fas fa-flag'></i>";
            return false;
        } else if (!placeFlag) {
            item.innerHTML = "<i class='fas fa-bomb'></i>";
            return true;
        }
    }

    UpdateFlagCount(i) {
        this.flagCount += i;
        this.h1Div.innerHTML = "<i class='fas fa-flag'></i> " + String(this.flagCount);
    }

    SetFlagCount(i) {
        this.flagCount = this.bombCount;
        this.h1Div.innerHTML = "<i class='fas fa-flag'></i> " + String(this.flagCount);
    }
}